import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppinglistService } from '../shopping-list/shoppinglist.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  recipesChanged = new Subject<Recipe[]>();
  private recipes : Recipe [] = []

  constructor(
    private shoppingService: ShoppinglistService
  ) { }


  // recipeSelected = new EventEmitter<Recipe>();
  // recipeSelected = new Subject<Recipe>();


  // private recipes: Recipe[] = [
  //   new Recipe("Test Recipe",
  //     "This is simply a test recipe",
  //     "https://cdn-image.myrecipes.com/sites/default/files/styles/4_3_horizontal_-_1200x900/public/mrtrending0475.jpg?itok=-tA_cB-C",
  //     [
  //       new Ingredient('Meat', 3),
  //       new Ingredient('French fries', 20),
  //       new Ingredient('Tomatoes', 3),
  //       new Ingredient('Spice', 10),
  //     ]),

  //   new Recipe("Second Recipe",
  //     "The delicious Lahmacun",
  //     "https://www.thespruceeats.com/thmb/yRrrie5HNGEiJQ00T49cebqnEW4=/960x0/filters:no_upscale():max_bytes(150000):strip_icc()/148517936_Greg-Elms-56a9946b3df78cf772a88016.jpg",
  //     [
  //       new Ingredient('Meat', 3),
  //       new Ingredient('Wrap', 20),
  //       new Ingredient('Tomatoes', 3),
  //       new Ingredient('Spice', 10),
  //     ]),
  // ]

  setRecipes(recipes: Recipe[]){
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }


  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(id: number) {
    return this.recipes[id];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingService.addIngredients(ingredients);

  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());

  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());

  }

  deleteRecipe(index:number){
    this.recipes.splice(index,1);
    this.recipesChanged.next(this.recipes.slice());
  }
}
