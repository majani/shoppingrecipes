import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from './auth.service';
import { ThrowStmt } from '@angular/compiler';
import { Observable } from 'rxjs';
import { AuthResponseData } from './auth.response.data';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  isLogInMode: boolean = true;
  isLoading: boolean = false;
  error: string = null;

  constructor(
    private authSvc: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSwitchMode() {
    this.isLogInMode = !this.isLogInMode;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;

    let authObs: Observable<AuthResponseData>;

    this.isLoading = true;

    if (this.isLogInMode) {
      authObs = this.authSvc.logIn(email, password);
    }

    else { //signup logic
      authObs = this.authSvc.signUp(email, password);

    }

    authObs.subscribe(
      response => {
        console.log(response);
        this.isLoading = false;
        this.router.navigate(['/recipes']);
      },
      errorMessage => {
        console.log(errorMessage);
        this.error = errorMessage;
        this.isLoading = false;
      }
    )




    console.log(form.value)
    form.reset();

  }

  onHandleError(){
    this.error = null;
  }

}
