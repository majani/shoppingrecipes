import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthResponseData } from './auth.response.data';
import { catchError, tap } from 'rxjs/operators';
import { throwError, BehaviorSubject } from 'rxjs';
import { User } from './user.model';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user = new BehaviorSubject<User>(null);
  private tokenExpirationTimer:any;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


  signUp(email:string, password:string){
    return this.http.post<AuthResponseData>("https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyA2H4pfvRQiK7HJxAxtwhOHBuwmfsqqSuU", {
      email: email,
      password: password,
      returnSecureToken: true

    }
    ).pipe(
      catchError(this.handleError), 
      tap( resData  =>{
        this.handleAuthentication(resData.email,resData.localId,resData.idToken,+resData.expiresIn);
      
    }) )
  }


  logIn(email:string, password:string){
    return this.http.post<AuthResponseData>('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyA2H4pfvRQiK7HJxAxtwhOHBuwmfsqqSuU', {
      email: email,
      password: password,
      returnSecureToken: true
    }).pipe(
      catchError(this.handleError),
      tap( resData  =>{
        this.handleAuthentication(resData.email,resData.localId,resData.idToken,+resData.expiresIn);
      
    }
    ))
  }

  logOut(){
    this.user.next(null);
    this.router.navigate(['/auth']);
    sessionStorage.removeItem('userData');

    if(this.tokenExpirationTimer){
      clearTimeout(this.tokenExpirationTimer)
    }
    this.tokenExpirationTimer =  null;
  }

  autoLogIn(){
    const userData: {
      email:string;
      id:string;
      _token:string;
      _tokenExpirationDate:string;

    } = JSON.parse(sessionStorage.getItem('userData'));

    if(!userData){
      return;
    }

    const loadedUser = new User(userData.email,userData.id, userData._token, new Date(userData._tokenExpirationDate));

    if(loadedUser.token){
      this.user.next(loadedUser);
      const expirationDuration = new Date(userData._tokenExpirationDate).getTime()-new Date().getTime();
      this.autoLogOut(expirationDuration);
    }
  }

  autoLogOut(expirationDuration:number){
    console.log(expirationDuration);
    this.tokenExpirationTimer = setTimeout(() => {
      this.logOut();
    }, 360000);

  }


  private handleError(errorRes: HttpErrorResponse){
    let errorMessage = "An Unknown Error Occured";

    if(!errorRes.error || !errorRes.error.error){
      throwError(errorMessage);
    }
    else {
      switch (errorRes.error.error.message) {
        case 'EMAIL_EXISTS': errorMessage = "This Email Already Exists";
        break
        case 'EMAIL_NOT_FOUND': errorMessage = "this Email Does not Exist. Please sign up";
        break;
        case 'INVALID_PASSWORD': errorMessage = "Invalid Password";
      }

      return throwError(errorMessage);

    }
  }

  private handleAuthentication(email:string, userId:string, token:string, expiresIn: number){
    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
      const user = new User(email, userId, token, expirationDate);
      this.user.next(user);
      this.autoLogOut(expiresIn * 1000)
      sessionStorage.setItem('userData', JSON.stringify(user));

  }
}
