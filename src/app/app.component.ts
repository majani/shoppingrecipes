import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { LoggingService } from './logging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'shoppingrecipes';

  constructor(
    private authSvc:AuthService,
    private logSvc: LoggingService
  ){}


  ngOnInit(){
    this.authSvc.autoLogIn();
    this.logSvc.printLog('Hello from Appcomponent')

  }
}
