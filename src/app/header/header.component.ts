import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { DataStorageService } from '../shared/data-storage.service';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuthenticated: boolean = false;
  userSub: Subscription;

  // @Output() featureSelected = new EventEmitter<string>();

  constructor(
    private dataSvc: DataStorageService,
    private authSvc: AuthService
  ) { }

  ngOnInit() {
    this.userSub = this.authSvc.user.subscribe(
      user => {
        //this.isAuthenticated = !user ? false : true;
        this.isAuthenticated = !!user;

      }
    );
  }

  ngOnDestroy(){
    this.userSub.unsubscribe();
  }
 

  onSaveData(){
    this.dataSvc.storeRecipes();
  }

  onFetchData(){
    this.dataSvc.fetchRecipes().subscribe()
  }

  onLogOut(){
    this.authSvc.logOut();
  }


  // onSelect(feature: string) {
  //   this.featureSelected.emit(feature);

  // }


}
